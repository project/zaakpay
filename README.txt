ZAAKPAY PAYMENT
--------------------

Zaakpay is a payment gateway to Drupal commerce. This module integrates with payment and allows easy and flexible online payment options.

This module creates a bridge with payment commerce module (https://www.drupal.org/project/payment_commerce) and responds the payment transaction object.

REQUIREMENTS
------------

This module requires the following dependencies:

 * Commerce
 * Payment Commerce
 * Payment


CONFIGURATION
-------------

 * Enable the module (Zaakpay for payment) - Administration » Modules.

 * Configure payment options in Administration » Configuration » Webservices » payment. 
     * Merchant Identifier   
     * Secret Key
 
 * Enable payment method MobiKwik - ZaakPay.
 

INSTALLATION
------------

  Please read INSTALL.txt file on how to install the module.
