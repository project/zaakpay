<?php

/**
 * Contains Zaakpay payment method controller.
 */

/**
 * A MobiKwik(ZaakPay) payment method.
 */
class PaymentMethodZaakpayController extends PaymentMethodController {

  /**
   * The production server.
   */
  const SERVER_PRODUCTION = 1;

  /**
   * The testing server.
   */
  const SERVER_TESTING = 0;

  /**
   * The production server URL.
   */
  const SERVER_PRODUCTION_URL = 'https://api.zaakpay.com/api/paymentTransact/V7';

  /**
   * The testing server URL.
   */
  const SERVER_TESTING_URL = 'http://zaakpaystaging.centralindia.cloudapp.azure.com:8080/api/paymentTransact/V7';

  public $controller_data_defaults = array(
    'merchantIdentifier' => '',
    'secret_key' => '',
    'merchantIpAddress' => '',
    'mode' => self::SERVER_PRODUCTION,
  );
    
  public $payment_method_configuration_form_elements_callback = 'zaakpay_payment_method_configuration_form_elements';

  function __construct() {
    $this->title = t('ZaakPay');
    $this->description = t("A MobiKwik Payment Gateway.");
  }

  /**
   * Implements PaymentMethodController::validate().
   * 
   * Todo.
   */
  function validate(Payment $payment, PaymentMethod $payment_method, $strict) {
        
  }

  /**
   * Implements PaymentMethodController::execute().
   */
  function execute(Payment $payment) {
    entity_save('payment', $payment);
    $_SESSION['zaakpay_pid'] = $payment->pid;
    drupal_goto('zaakpay/redirect/' . $payment->pid);
  }

  /**
   * Returns the API server URL.
   *
   * @throws InvalidArgumentException
   *
   * @param int $server
   *   self::SERVER_PRODUCTION or self::SERVER_TESTING.
   *
   * @return string
   */
  function serverURL($server) {
    $urls = array(
      $this::SERVER_PRODUCTION => $this::SERVER_PRODUCTION_URL,
      $this::SERVER_TESTING => $this::SERVER_TESTING_URL,
    );
    
    if (array_key_exists($server, $urls)) {
      return url($urls[$server], array(
        'external' => TRUE,
      ));
    }
    else {
      throw new InvalidArgumentException(t('Server type does not exist.'));
    }
  }
    
  /**
   * Sets up redirect POST data.
   *
   * @param Payment $payment
   *
   * @return array
   *   Keys are POST parameter names, values are values.
   */
  function requestData(Payment $payment) {
    $return_url = url('zaakpay/response', array(
      'absolute' => TRUE,
    ));
    $payment->currency_code = isset($payment->currency_code) ? $payment->currency_code : 'INR';
    
    // Get commerce order object.
    if ($payment->context == 'payment_commerce') {
      $order = commerce_order_load($payment->context_data['order_id']);
      $order_wrapper = entity_metadata_wrapper('commerce_order', $order);
      $address = $order_wrapper->commerce_customer_billing->commerce_customer_address->value();
      $mobile = $order_wrapper->commerce_customer_billing->field_phone_number->value();
      $payment->context_data['buyerEmail'] = $order->mail;
      $payment->context_data['buyerFirstName'] = $address['first_name'];
      $payment->context_data['buyerLastName'] = $address['last_name'];
      $payment->context_data['buyerAddress'] = $address['thoroughfare'] . '-' . $address['premise'];
      $payment->context_data['buyerCity'] = $address['locality'];
      $payment->context_data['buyerState'] = $address['administrative_area'];
      $payment->context_data['buyerCountry'] = $address['country'];
      $payment->context_data['buyerPincode'] = $address['postal_code'];
      $payment->context_data['buyerPhoneNumber'] = $mobile;
    }
    
    // Prepare request data.
    $data = array(
      'merchantIdentifier' => $payment->method->controller_data['merchantIdentifier'],
      'orderId' => $payment->pid,
      'returnUrl' => $return_url,
      'buyerEmail' => $payment->context_data['buyerEmail'],
      'buyerFirstName' => $payment->context_data['buyerFirstName'],
      'buyerLastName' => $payment->context_data['buyerLastName'],
      'buyerAddress' => $payment->context_data['buyerAddress'],
      'buyerCity' => $payment->context_data['buyerCity'],
      'buyerState' => $payment->context_data['buyerState'],
      'buyerCountry' => $payment->context_data['buyerCountry'],
      'buyerPincode' => $payment->context_data['buyerPincode'],
      'buyerPhoneNumber' => $payment->context_data['buyerPhoneNumber'],
      'txnType' => '1',
      'zpPayOption' => '1',
      'mode' => $payment->method->controller_data['mode'],
      'currency' => $payment->currency_code,
      'amount' => $payment->totalAmount(TRUE) * 100,
      'merchantIpAddress' => $payment->method->controller_data['merchantIpAddress'],
      'purpose' => '1',
      'productDescription' => $payment->description,
      'txnDate' => date('Y-m-d', REQUEST_TIME)
    );
    
    // Sanitize post data.
    foreach ($data as $key => &$value) {
      if ($key == 'returnUrl') {
        $value = $this->sanitizedURL($value);
      }
      else {
        $value = $this->sanitizedParam($value);
      }
    }
    return $data;
  }

  /**
   * Calculate checksum value.
   * 
   * @return checksum.
   */
  function calculateChecksum(Payment $payment) {
    $data = $this->requestData($payment);
    $secret_key = $payment->method->controller_data['secret_key'];
    $hash = hash_hmac('sha256', $this->getAllParams($data), $secret_key);
    $checksum = $hash;
    return $checksum;
  }
    
  /**
   * Get Response data.
   */
  function responseData() {
    $data = drupal_get_query_parameters($_POST);
    return $data;
  }
    
  /**
   * Process response data & set payment status.
   */
  function processResponse(array $data, Payment $payment) {
    switch ($data['responseCode']) {
      case '100':
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_SUCCESS));
        break;
        
      case '102':
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_CANCELLED));
        break;
        
      default:
        $payment->setStatus(new PaymentStatusItem(PAYMENT_STATUS_FAILED));
    }
    $payment->finish();
  }

  /**
   * TODO.
   */
  protected function getAllParams(array $params) {
    $all = '';

    $checksumsequence = array("amount", "bankid", "buyerAddress",
    "buyerCity", "buyerCountry", "buyerEmail", "buyerFirstName", "buyerLastName", "buyerPhoneNumber", "buyerPincode",
    "buyerState", "currency", "debitorcredit", "merchantIdentifier", "merchantIpAddress", "mode", "orderId",
    "product1Description", "product2Description", "product3Description", "product4Description",
    "productDescription", "productInfo", "purpose", "returnUrl", "shipToAddress", "shipToCity", "shipToCountry",
    "shipToFirstname", "shipToLastname", "shipToPhoneNumber", "shipToPincode", "shipToState", "showMobile", "txnDate",
    "txnType", "zpPayOption");

    foreach ($checksumsequence as $seqvalue) {
      if (array_key_exists($seqvalue, $params)) {
        if (isset($params[$seqvalue])) {
          if ($seqvalue != 'checksum') {
            $all .= $seqvalue;
            $all .="=";
            $all .= $params[$seqvalue];
            $all .= "&";
          }
        }
      }
    }
    return $all;
  }

  /**
   * Sanitize post form data.
   * 
   * @return string.
   */
  protected function sanitizedParam($param) {
    $pattern[0] = "%,%";
    $pattern[1] = "%#%";
    $pattern[2] = "%\(%";
    $pattern[3] = "%\)%";
    $pattern[4] = "%\{%";
    $pattern[5] = "%\}%";
    $pattern[6] = "%<%";
    $pattern[7] = "%>%";
    $pattern[8] = "%`%";
    $pattern[9] = "%!%";
    $pattern[10] = "%\\$%";
    $pattern[11] = "%\%%";
    $pattern[12] = "%\^%";
    $pattern[13] = "%=%";
    $pattern[14] = "%\+%";
    $pattern[15] = "%\|%";
    $pattern[16] = "%\\\%";
    $pattern[17] = "%:%";
    $pattern[18] = "%'%";
    $pattern[19] = "%\"%";
    $pattern[20] = "%;%";
    $pattern[21] = "%~%";
    $pattern[22] = "%\[%";
    $pattern[23] = "%\]%";
    $pattern[24] = "%\*%";
    $pattern[25] = "%&%";
    $sanitizedParam = preg_replace($pattern, "", $param);
    return $sanitizedParam;
  }
      
  /**
   * Sanitize url.
   * 
   * @return string
   */
  protected function sanitizedURL($param) {
    $pattern[0] = "%,%";
    $pattern[1] = "%\(%";
    $pattern[2] = "%\)%";
    $pattern[3] = "%\{%";
    $pattern[4] = "%\}%";
    $pattern[5] = "%<%";
    $pattern[6] = "%>%";
    $pattern[7] = "%`%";
    $pattern[8] = "%!%";
    $pattern[9] = "%\\$%";
    $pattern[10] = "%\%%";
    $pattern[11] = "%\^%";
    $pattern[12] = "%\+%";
    $pattern[13] = "%\|%";
    $pattern[14] = "%\\\%";
    $pattern[15] = "%'%";
    $pattern[16] = "%\"%";
    $pattern[17] = "%;%";
    $pattern[18] = "%~%";
    $pattern[19] = "%\[%";
    $pattern[20] = "%\]%";
    $pattern[21] = "%\*%";
    $sanitizedParam = preg_replace($pattern, "", $param);
    return $sanitizedParam;
  }
}
